MATCH (v:Violator)
WITH v, SIZE((v)<-[:issued_to]-()) as orderCnt
ORDER BY orderCnt DESC LIMIT 3
MATCH (o)-[:issued_to]->(v)
RETURN o, v