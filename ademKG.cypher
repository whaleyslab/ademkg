LOAD CSV WITH HEADERS FROM 'file:///chw_orders.csv' AS row
MERGE (f:FiscalYear {name: row.FISCAL_YEAR})
MERGE (p:Program {name: row.PROGRAM})
MERGE(o:Order{id:row.ORDER_NO, issue_date:date(datetime({epochMillis:apoc.date.parse(row.DATE_ISSUED,'ms', 'MM/dd/yyyy')})), penalty:toFloat(row.PENALTY)})
MERGE(v:Violator{name: row.NAME_OF_VIOLATOR, address: row.ADDRESS, city: row.CITY, county: row.COUNTY, state: row.STATE, latitude: toFloat(row.Y), longitude: toFloat(row.X)})
MERGE(p)<-[:infracting]-(v)<-[:issued_to]-(o)<-[:contains]-(f)
RETURN f,p,o,v
